﻿
using BitMiracle.LibTiff.Classic;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Wander
{
    /* Usage:
     * 
     * 1. var heightData = GeoTiffHeight.Load( filename, filter=true );
     * 2. var asyncHeightData = GeoTiffHeight.Load( url, filter=true );
     *      then keep calling: asyncHeightData.IsFinished( .. )
     * float h = heightData.FromRD( rdX, rdY );
     * NOTE: RD-coordinates: https://nl.wikipedia.org/wiki/Rijksdriehoeksco%C3%B6rdinaten
     * Convert GPS to RD (e.g from Google maps):  https://www.gpscoordinaten.nl/converteer-rd-coordinaten.php
     * */

    public struct HeightData
    {
        public float[] data;
        public int width;
        public int height;
        public double originX;
        public double originY;
        public double pixelSizeX;
        public double pixelSizeY;

        public float FromRD( double x, double y, out bool wasFound, bool valideHeight = true )
        {
            float h = 0;
            wasFound = false;
            int xPixel = Mathf.FloorToInt( (float)((x-originX) / pixelSizeX ) );
            int yPixel = Mathf.FloorToInt( (float)((y-originY) / pixelSizeY ) );
            if (xPixel >= 0 && xPixel<width && yPixel >= 0 && yPixel<height)
            {
                h = data[yPixel*width+xPixel];
                if (valideHeight)
                {
                    GeoTiffHeight.Validate( ref h );
                }
                wasFound = true;
            }
            return h;
        }

        public float FromUnity( Vector2 unityPos, Vector2 rdCentre, out bool wasFound, bool valideHeight = true )
        {
            unityPos -= rdCentre;
            return FromRD( unityPos.x, unityPos.y, out wasFound, valideHeight );
        }
    }

    public class AsyncHeightData
    {
        internal HeightData heightData;
        internal bool valid;
        internal int  numRings;
        internal bool filter;
        internal float couldNotSolveValue;
        internal UnityWebRequest www;
        internal string url;
        internal bool started;
        internal bool retried;
        internal Task<HeightData> decodeTiffBytesTask;

        public HeightData HeightData => heightData;
        public bool Valid => valid;
        public bool Retried => retried;

        public bool Started => started;

        public void Start()
        {
            if (www!=null)
            {
                Debug.Assert( !started, "Already started" );
                www.SendWebRequest();
                started = true;
            }
        }

        public void Retry()
        {
            Debug.Assert( started );
            Debug.Assert( !retried, "Already retried" );
            www = UnityWebRequest.Get( url );
            www.SendWebRequest();
            retried = true;
        }

        public bool IsFinished()
        {
            if (www == null)
                return true;

            // Still in progress
            if (www.result == UnityWebRequest.Result.InProgress) // If there is an error, then Done is not set true..
            {
                if (!www.isDone)
                    return false;
            }

            // Either isDone is true, or result is error but not InProgress.

            // Request is done, but not valid.
            if (www.result != UnityWebRequest.Result.Success)
                return true;

            var tiffBytes = www.downloadHandler.data;
            if ( decodeTiffBytesTask == null )
            {
                bool f=filter;
                int nRings=numRings;
                float invSample = couldNotSolveValue;
                decodeTiffBytesTask = Task.Run( () =>
                {
                    float [] data = null;
                    int width =0, height=0;
                    double originX=0, originY=0;
                    double pixelSizeX=0, pixelSizeY=0;
               
                    data = GeoTiffHeight.DecodePixelsAsFloat( tiffBytes,
                        out width, out height,
                        out originX, out originY,
                        out pixelSizeX, out pixelSizeY );

                    tiffBytes = null;
                    data = GeoTiffHeight.FilterIfWanted( filter, nRings, invSample, data, width, height );

                    return new HeightData()
                    {
                        data = data,
                        width = width,
                        height = height,
                        originX = originX,
                        originY = originY,
                        pixelSizeX = pixelSizeX,
                        pixelSizeY = pixelSizeY
                    };

                } );
            }
            else if ( decodeTiffBytesTask.IsCompleted )
            {
                if ( decodeTiffBytesTask.IsCompletedSuccessfully )
                {
                    heightData = decodeTiffBytesTask.Result;
                    valid = true;
                }
                decodeTiffBytesTask = null;
                www = null;
                return true;
            }

            return false;
        }
    }

    public class GeoTiffHeight
    {
        private static float InvalidLow  = -90;
        private static float InvalidHigh = 9000;

        public static string BuildPDOKWCSUrl( double rdMinX, double rdMaxX, double rdMinY, double rdMaxY, int resWidth = 1024, int resHeight = 1024 )
        {
            string url = $"https://service.pdok.nl/rws/ahn/wcs/v1_0?SERVICE=WCS&VERSION=1.0.0&REQUEST=GetCoverage&coverage=dtm_05m&CRS=EPSG:28992&BBOX={rdMinX},{rdMinY},{rdMaxX},{rdMaxY}&WIDTH={resWidth}&HEIGHT={resHeight}&FORMAT=image/tiff&interpolation=average";
            return url;
        }

        // couldNotSolveValue: what to put if no sample could be found.
        public static AsyncHeightData LoadFromUrl( string url, bool filter = true, int numRings = 10, float couldNotSolveValue=0, bool startImmediate=true )
        {
            AsyncHeightData asyncDataOperation = new AsyncHeightData();
            asyncDataOperation.filter   = filter;
            asyncDataOperation.numRings = numRings;
            asyncDataOperation.couldNotSolveValue = couldNotSolveValue;
            asyncDataOperation.www = UnityWebRequest.Get( url );
            asyncDataOperation.url = url;
            if (startImmediate)
            {
                asyncDataOperation.www.SendWebRequest();
                asyncDataOperation.started = true;
            }
            return asyncDataOperation;
        }

        public static HeightData Load( string filename, bool filter = true, int numFilterRings = 5, float couldNotSolveValue=0)
        {
            byte [] tiffBytes = System.IO.File.ReadAllBytes( filename );

            var data = DecodePixelsAsFloat( tiffBytes,
                out int width, out int height,
                out double originX, out double originY,
                out double pixelSizeX, out double pixelSizeY );

            data = FilterIfWanted( filter, numFilterRings, couldNotSolveValue, data, width, height );

            return new HeightData()
            {
                data = data,
                width = width,
                height = height,
                originX = originX,
                originY = originY,
                pixelSizeX = pixelSizeX,
                pixelSizeY = pixelSizeY
            };
        }

        public static float[] FilterIfWanted( bool filter, int numFilterRings, float couldNotSolveValue, float[] data, int width, int height )
        {
            if (data != null && filter)
            {
                if (data.Any( v => IsSane(v)) ) // If at least a single value is valid, we can solve it.
                {
                    bool hasInvalidSamples;
                    do
                    {
                        data = FilterFloatData2( data, width, height, numFilterRings, out hasInvalidSamples );
                    } while (hasInvalidSamples);
                }
                else
                {
                    for (int i = 0;i<width*height;i++)
                        data[i] = couldNotSolveValue;
                }
            }

            return data;
        }

        public static void Validate(ref float h)
        {
            if (h < InvalidLow)
            {
                h = 0;
                return;
            }
            if (h > InvalidHigh) h = 0;
        }

        internal static float[] DecodePixelsAsFloat( 
            byte [] tiffBytes, out int width, out int height,
            out double originX, out double originY,
            out double pixelSizeX, out double pixelSizeY 
            )
        {
            width  = 0;
            height = 0;
            float[] output = null;

            TiffStreamForBytes ts = new TiffStreamForBytes(tiffBytes);

            using (Tiff tiff = Tiff.ClientOpen( "bytes", "r", null, ts ))
            {
                if (tiff == null)
                {
                    throw new InvalidDataException( "Tiff data not valid" );
                }

                // Obtain coords
                {
                    FieldValue[] modelPixelScaleTag = tiff.GetField((TiffTag)33550);
                    FieldValue[] modelTiepointTag   = tiff.GetField((TiffTag)33922);

                    byte[] modelPixelScale = modelPixelScaleTag[1].GetBytes();
                    pixelSizeX = BitConverter.ToDouble( modelPixelScale, 0 );
                    pixelSizeY = BitConverter.ToDouble( modelPixelScale, 8 ) * -1;

                    byte[] modelTransformation = modelTiepointTag[1].GetBytes();
                    originX = BitConverter.ToDouble( modelTransformation, 24 );
                    originY = BitConverter.ToDouble( modelTransformation, 32 );
                }

                
                width  = tiff.GetField( TiffTag.IMAGEWIDTH )[0].ToInt();
                height = tiff.GetField( TiffTag.IMAGELENGTH )[0].ToInt();
                output = new float[width*height];
                int tileSize = tiff.TileSize();

                if (tiff.IsTiled())
                {
                    byte [] tileData = new byte[tileSize];
                    int tile_width   = tiff.GetField( TiffTag.TILEWIDTH )[0].ToInt();
                    int tile_height  = tiff.GetField( TiffTag.TILELENGTH )[0].ToInt();

                    for (int row = 0;row < height;row += tile_height) for (int col = 0;col < width;col += tile_width)
                    {
                        // Read the tile into an RGBA array
                        int res = tiff.ReadTile( tileData, 0, col, row, 0, 0 );
                        if (res == -1)
                            throw new InvalidDataException( "Invalid tiff tile" );

                        for (int y = 0;y < tile_height;y++)
                        {
                            int y2 = row + y;
                            if (y2 >= height)
                                continue;
                            for (int x = 0;x < tile_width;x++)
                            {
                                int x2 = col + x;
                                if (x2 >= width)
                                    continue;

                                float h = BitConverter.ToSingle( tileData, (y * tile_width + x) * 4 );
                                output[y2*width + x2] = h;
                            }
                        }
                    }
                }
                else
                {
                    int samplesPerPixel = tiff.GetField(TiffTag.SAMPLESPERPIXEL)[0].ToInt();
                    int bitsPerSample   = tiff.GetField(TiffTag.BITSPERSAMPLE)[0].ToInt();
                    Debug.Assert( samplesPerPixel == 1 && bitsPerSample == 32, "Expecting 32-bit floating point texture. Others are not handled." );
                    int scanlineSize = tiff.ScanlineSize();
                    byte [] scanline = new byte[scanlineSize];
                    for (int i = 0;i < height;++i)
                    {
                        tiff.ReadScanline( scanline, i );
                        Buffer.BlockCopy( scanline, 0, output, i*width*4, scanlineSize );
                    }
                }
            }

            return output;
        }

        public static bool IsSane( float sample )
        {
            return sample >= InvalidLow && sample <= InvalidHigh &&
                !float.IsInfinity( sample ) && !float.IsNaN( sample );
        }

        internal static float [] FilterFloatData2( float[] data, int width, int height, int numRings, out bool hasInvalidSamples )
        {
            hasInvalidSamples = false;
            float [] result = new float[width*height];
            
            for (int y = 0;y<height;y++)
            {
                for (int x = 0;x<width;x++)
                {
                    int addr = y*width+x;
                    float sample = data[addr];

                    // If is valid, continue.
                    if (IsSane( sample ))
                    {
                        result[addr] = sample;
                        continue;
                    }
                    hasInvalidSamples = true;

                    float sampleSum = 0;    
                    float distSum = 0;
                    for (int y2 = -numRings;y2<=numRings;y2++)
                    {
                        for (int x2 = -numRings;x2<=numRings;x2++)
                        {
                            int x3 = x + x2;
                            int y3 = y + y2;

                            if (x==x3 && y==y3)
                                continue; // Skip self.

                            if (x3 < 0 || x3 >= width || y3 < 0 || y3 >= height)
                                continue; // Out of bounds.

                            int addr2 = y3 * width + x3;
                            sample = data[addr2];

                            if (!IsSane(sample))
                                continue; // Also invalid sample.

                            float dx = x-x3;
                            float dy = y-y3;
                            float len = Mathf.Sqrt( dx*dx + dy*dy );
                            Debug.Assert( len > 0 );
                            float weight = 1.0f / (len*len);
                            distSum += weight;
                            sampleSum += sample * weight;
                        }
                    }

                    result[addr] = float.NegativeInfinity;
                    if (distSum != 0)
                    {
                        sampleSum /= distSum; // Needs normalization.
                        if (IsSane( sampleSum ))
                        {
                            result[addr] = sampleSum;
                        }
                    }
                }
            }

            return result;
        }
    }
}