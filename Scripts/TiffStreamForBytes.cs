using System;
using System.IO;
using BitMiracle.LibTiff.Classic;


namespace Wander
{
    /// <summary>
    /// Custom read-only stream for byte buffer that can be used
    /// with Tiff.ClientOpen method.
    /// </summary>
    class TiffStreamForBytes : TiffStream
    {
        private byte[] m_bytes;
        private int m_position;

        public TiffStreamForBytes( byte[] bytes )
        {
            m_bytes = bytes;
            m_position = 0;
        }

        public override int Read( object clientData, byte[] buffer, int offset, int count )
        {
            if ( (m_position + count) > m_bytes.Length )
                return -1;

            Buffer.BlockCopy( m_bytes, m_position, buffer, offset, count );
            m_position += count;
            return count;
        }

        public override void Write( object clientData, byte[] buffer, int offset, int count )
        {
            throw new InvalidOperationException( "This stream is read-only" );
        }

        public override long Seek( object clientData, long offset, SeekOrigin origin )
        {
            switch ( origin )
            {
                case SeekOrigin.Begin:
                    if ( offset > m_bytes.Length )
                        return -1;

                    m_position = (int)offset;
                    return m_position;

                case SeekOrigin.Current:
                    if ( (offset + m_position) > m_bytes.Length )
                        return -1;

                    m_position += (int)offset;
                    return m_position;

                case SeekOrigin.End:
                    if ( (m_bytes.Length - offset) < 0 )
                        return -1;

                    m_position = (int)(m_bytes.Length - offset);
                    return m_position;
            }

            return -1;
        }

        public override void Close( object clientData )
        {
            // nothing to do
        }

        public override long Size( object clientData )
        {
            return m_bytes.Length;
        }
    }
}