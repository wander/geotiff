## GeoTiff

Can load GeoTiff data and provides acces to its coordinates and encoded pixel data (often height).

### Dependencies
Has a dependency to BitMiracle.LibTiff.Net but this package is embedded, so no action required.

### Usage 1 (from file)

```
var heightData = GeoTiffHeight.Load( filename, filter=true );
float h = heightData.FromRD( rdX, rdY );

```

### Usage 2 (from url)
```
var url = GeoTiffHeight.BuildPDOKWCSUrl( xMin, xMax, yMin, yMx, resWidth=1024, resHeight=1024 );
var asyncHeightData = GeoTiffHeight.LoadFromUrl( url, filter=true );
var HeightData heightData;
while (!asyncHeightData.Finished(ref heightData, out bool isValid))
{
	.. wait interval ..
}
if (isValid)
{
	float h = heightData.FromRD( rdX, rdY );
}

```

### Note
- RD-coordinates: https://nl.wikipedia.org/wiki/Rijksdriehoeksco%C3%B6rdinaten
- Convert GPS to RD (e.g from Google maps):  https://www.gpscoordinaten.nl/converteer-rd-coordinaten.php
